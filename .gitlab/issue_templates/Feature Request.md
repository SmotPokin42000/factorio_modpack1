Read, and then remove this notice from your feature request.

Please make sure the feature you are requesting is not a duplicate, or anything that has been implemented since the version of the code which you are running.

Please fill out the information below to the best of your ability.

------

### Update or new functionality?

(Please let us know if this request is an update to an existing module/function, or if this is to build a completely new module/function.)

### Description

(Include problem, use case, benefits, and/or goals)

### Complete Request

(Describe in some depth what changes are requested.  Remember to include not only the quiet functionality, but descriptions or links to images depicting GUI functionality.)
